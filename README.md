# Evenger API 

REST-API for Event Manager (Evenger) built with Adonis JS v5. Evenger is an activity management application.

## Users

### Participant
- Registration & login
- Register events
- Get event results

### User Organizer
- Manage an organizer
- Making events
- Validate event registrants
- Submit event result to participants

### Operator 
- Manage organizer
- Manage user organizer
- Validate events

### Admin
- Manage departments
- Manage operators

## Installation

Evenger API requires Node.js v14 & NPM v6 to run.

```sh
git clone https://gitlab.com/evenger/evenger-api evenger-api
cd evenger-api
npm install && npm update
cp .env.example .env
node ace generate:key
node ace migration:run
node ace db:seed
node ace serve --watch
```