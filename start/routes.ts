import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { data: 'welcome to Evenger API' }
})

Route.post('login', 'AuthController.login')
Route.post('registration', 'ParticipantsController.store')

Route.group(() => {
  Route.resource('admin', 'AdminsController').apiOnly()

  Route.resource('operator', 'OperatorsController').apiOnly()

  Route.resource('participant', 'ParticipantsController').apiOnly()

  Route.resource('user-organizer', 'UserOrganizersController').apiOnly()

  Route.resource('department', 'DepartmentsController').apiOnly()

  Route.resource('organizer', 'OrganizersController').apiOnly()

  Route.post('event/validate', 'EventsController.validate')
  Route.resource('event', 'EventsController').apiOnly()

  Route.post('event-registrant/submit-result', 'EventRegistrantsController.submitResult')
  Route.post('event-registrant/validate', 'EventRegistrantsController.validate')
  Route.resource('event-registrant', 'EventRegistrantsController').apiOnly()

  Route.post('logout', 'AuthController.logout')
}).middleware('auth')

