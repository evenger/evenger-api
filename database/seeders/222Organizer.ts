import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Organizer from 'App/Models/Organizer'
import faker from 'faker'

export default class OrganizerSeeder extends BaseSeeder {
  public async run () {

    await Organizer.createMany([
      { name: 'Riset Teknologi Informasi', departmentId: 1, desc: faker.lorem.paragraphs(), logo: faker.image.image() },
      { name: 'Senat Teknik', departmentId: 2, desc: faker.lorem.paragraphs(), logo: faker.image.image() },
      { name: 'Senat Ekonomi', departmentId: 3, desc: faker.lorem.paragraphs(), logo: faker.image.image() },
      { name: 'HMB BASICA', departmentId: 4, desc: faker.lorem.paragraphs(), logo: faker.image.image() },
      { name: 'HIMAKOM', departmentId: 5, desc: faker.lorem.paragraphs(), logo: faker.image.image() },
      { name: 'HMPS iSystem', departmentId: 6, desc: faker.lorem.paragraphs(), logo: faker.image.image() },
      { name: 'HMPS Akuntansi', departmentId: 7, desc: faker.lorem.paragraphs(), logo: faker.image.image() },
      { name: 'GenBI', departmentId: 8, desc: faker.lorem.paragraphs(), logo: faker.image.image() },
    ])

  }
}
