import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from 'App/Models/User'
import faker from 'faker'

export default class UserSeeder extends BaseSeeder {
  public async run () {

    faker.locale = 'id_ID'

    // admin
    for (let index = 1; index <= 5; index++) {
      await User.create({
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: 'password',
        is_admin: true,
      })
    }

    // organizer
    for (let index = 1; index <= 8; index++) {
      await User.create({
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: 'password',
        organizerId: index,
      })
    }

    // operator
    for (let index = 1; index <= 8; index++) {
      await User.create({
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: 'password',
        departmentId: index,
      })
    }

    // participant
    for (let index = 1; index <= 25; index++) {
      await User.create({
        name: faker.name.findName(),
        email: faker.internet.email(),
        password: 'password',
      })
    }

  }
}
