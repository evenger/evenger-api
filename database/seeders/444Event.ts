import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Event from 'App/Models/Event'
import faker from 'faker'
import { EventState } from 'Contracts/enums'

export default class EventSeeder extends BaseSeeder {
  public async run () {

    faker.locale = 'id_ID'

    for (let index = 1; index <= 8; index++) {
      await Event.create({
        name: faker.address.streetAddress(),
        start: faker.datatype.datetime(),
        end: faker.datatype.datetime(),
        desc: faker.lorem.sentences(),
        poster: faker.image.image(),
        category: faker.lorem.word(),
        state: faker.helpers.randomize(Object.values(EventState)),
        organizerId: index,
      })
    }

  }
}
