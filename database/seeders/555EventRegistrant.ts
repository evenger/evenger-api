import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import EventRegistrant from 'App/Models/EventRegistrant'
import faker from 'faker'
import { EventRegistrantState } from 'Contracts/enums'

export default class EventRegistrantSeeder extends BaseSeeder {
  public async run () {

    faker.locale = 'id_ID'

    for (let index = 1; index <= 50; index++) {
      await EventRegistrant.create({
        eventId: faker.helpers.randomize([
          1, 2, 3, 4, 5, 6, 7, 8
        ]),
        userId: faker.helpers.randomize([
          1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25
        ]),
        state: faker.helpers.randomize(Object.values(EventRegistrantState)),
      })
    }

  }
}
