import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Department from 'App/Models/Department'
import { DepartmentType } from 'Contracts/enums'

export default class DepartmentSeeder extends BaseSeeder {
  public async run() {

      await Department.createMany([
        { name: 'Universitas Negeri Gorontalo', type: DepartmentType.UNIVERSITY },
        { name: 'Fakultas Teknik', type: DepartmentType.FACULTY },
        { name: 'Fakultas Ekonomi', type: DepartmentType.FACULTY },
        { name: 'Teknik Informatika', type: DepartmentType.DEPARTMENT },
        { name: 'Ilmu Komunikasi', type: DepartmentType.DEPARTMENT },
        { name: 'Sistem Informasi', type: DepartmentType.STUDY_PROGRAM },
        { name: 'Akuntansi', type: DepartmentType.STUDY_PROGRAM },
        { name: 'External', type: DepartmentType.EXTERNAL },
      ])

  }
}
