import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import { EventState } from 'Contracts/enums'

export default class Events extends BaseSchema {
  protected tableName = 'events'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name').notNullable()
      table.dateTime('start').notNullable()
      table.dateTime('end').notNullable()
      table.text('desc').nullable()
      table.string('poster').notNullable()
      table.string('category').notNullable()
      table.enum('state', Object.values(EventState)).defaultTo(EventState.PROCESSED)
      table.text('state_desc').nullable()
      table.integer('organizer_id').unsigned()
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
