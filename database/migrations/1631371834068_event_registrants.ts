import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import { EventRegistrantState } from 'Contracts/enums'

export default class EventRegistrants extends BaseSchema {
  protected tableName = 'event_registrants'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.enum('state', Object.values(EventRegistrantState)).defaultTo(EventRegistrantState.PROCESSED)
      table.text('state_desc').nullable()
      table.string('result_file').nullable()
      table.text('result_desc').nullable()
      table.integer('event_id').unsigned()
      table.integer('user_id').unsigned()
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
