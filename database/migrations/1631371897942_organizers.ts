import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Organizers extends BaseSchema {
  protected tableName = 'organizers'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name').notNullable()
      table.text('desc').nullable()
      table.string('logo').notNullable()
      table.integer('department_id').unsigned()
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
