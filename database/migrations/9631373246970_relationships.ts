import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Relationships extends BaseSchema {

  public async up () {
    this.schema.alterTable('users', (table) => {
      table.foreign('organizer_id').references('organizers.id').onDelete('cascade').onUpdate('cascade')
      table.foreign('department_id').references('departments.id').onDelete('cascade').onUpdate('cascade')
    })

    this.schema.alterTable('organizers', (table) => {
      table.foreign('department_id').references('departments.id').onDelete('cascade').onUpdate('cascade')
    })

    this.schema.alterTable('event_registrants', (table) => {
      table.foreign('event_id').references('events.id').onDelete('cascade').onUpdate('cascade')
      table.foreign('user_id').references('users.id').onDelete('cascade').onUpdate('cascade')
    })

    this.schema.alterTable('events', (table) => {
      table.foreign('organizer_id').references('organizers.id').onDelete('cascade').onUpdate('cascade')
    })

    this.schema.alterTable('api_tokens', (table) => {
      table.foreign('user_id').references('id').inTable('users').onDelete('CASCADE')
    })

  }

  public async down () {
    this.schema.alterTable('users', (table) => {
      table.dropForeign('organizer_id')
      table.dropForeign('department_id')
    })

    this.schema.alterTable('organizers', (table) => {
      table.dropForeign('department_id')
    })

    this.schema.alterTable('event_registrants', (table) => {
      table.dropForeign('event_id')
      table.dropForeign('user_id')
    })

    this.schema.alterTable('events', (table) => {
      table.dropForeign('organizer_id')
    })
  }
}
