import { schema } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class CreateOrganizerValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    name: schema.string({ trim: true }),

    desc: schema.string({ trim: true }),

    logo: schema.file({
      size: '5mb',
      extnames: ['jpg', 'gif', 'png'],
    }),
  })

  public messages = {}
}
