import { schema } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class CreateEventValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    name: schema.string({ trim: true }),

    start: schema.date({
      format: 'yyyy-MM-dd HH:mm:ss',
    }),

    end: schema.date({
      format: 'yyyy-MM-dd HH:mm:ss',
    }),

    desc: schema.string({ trim: true }),

    poster: schema.file({
      size: '5mb',
      extnames: ['jpg', 'gif', 'png'],
    }),

    category: schema.string({ trim: true }),
  })

  public messages = {}
}
