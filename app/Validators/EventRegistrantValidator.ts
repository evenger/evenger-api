import { schema } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class EventRegistrantValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    eventId: schema.number(),
  })

  public messages = {}
}
