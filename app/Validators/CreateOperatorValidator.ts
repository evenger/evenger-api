import { schema } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class CreateOperatorValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({

    name: schema.string({ trim: true }),

    password: schema.string({ trim: true }),

    departmentId: schema.number(),

    email: schema.string({ trim: true }),

  })

  public messages = {}
}
