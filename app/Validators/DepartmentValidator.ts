import { schema } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { DepartmentType } from 'Contracts/enums'

export default class DepartmentValidator {
  constructor (protected ctx: HttpContextContract) {
  }

  public schema = schema.create({
    name: schema.string({ trim: true }),

    type: schema.enum(Object.values(DepartmentType)),
  })

  public messages = {}
}
