import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import CreateOperatorValidator from 'App/Validators/CreateOperatorValidator'
import UpdateOperatorValidator from 'App/Validators/UpdateOperatorValidator'

export default class OperatorsController {

  // admin
  public async index({ request, bouncer }: HttpContextContract) {
    await bouncer
      .with('OperatorPolicy')
      .authorize('viewList')

    let page = request.input('page', 1)
    let name = request.input('name', '')

    let operators = await User.query()
      .preload('department')
      .whereNotNull('department_id')
      .if((name), (query) => {
        query.where('name', 'like', '%' + name + '%')
      })
      .orderBy('id', 'desc')
      .paginate(page, 10)

    return operators
  }

  // admin
  public async store({ request, response, bouncer }: HttpContextContract) {
    await bouncer
      .with('OperatorPolicy')
      .authorize('create')

    await request.validate(CreateOperatorValidator)

    let operator = await User.create({
      name: request.input('name'),
      email: request.input('email'),
      password: request.input('password'),
      departmentId: request.input('departmentId')
    })

    return response.json({
      data: operator
    })
  }

  // admin & operator itu sendiri
  public async show({ response, params, bouncer }: HttpContextContract) {
    let operator = await User.query()
      .preload('department')
      .whereNotNull('department_id')
      .where('id', params.id)
      .firstOrFail()

    await bouncer
      .with('OperatorPolicy')
      .authorize('view', operator)

    return response.json({
      data: operator
    })
  }

  // admin & operator itu sendiri
  public async update({ response, params, request, bouncer }: HttpContextContract) {
    await request.validate(UpdateOperatorValidator)

    let operator = await User.query()
      .whereNotNull('department_id')
      .where('id', params.id)
      .firstOrFail()

    await bouncer
      .with('OperatorPolicy')
      .authorize('view', operator)

    operator.name = request.input('name')
    operator.email = request.input('email')
    if (request.input('password')) operator.password = request.input('password')
    operator.departmentId = request.input('departmentId')
    await operator.save()

    return response.json({
      data: operator
    })
  }

  // admin
  public async destroy({ response, params, bouncer }: HttpContextContract) {
    await bouncer
      .with('OperatorPolicy')
      .authorize('delete')

    let operator = await User.query()
      .whereNotNull('department_id')
      .where('id', params.id)
      .firstOrFail()

    await operator.delete()

    return response.json({
      data: 'Operator deleted successfully'
    })

  }

}
