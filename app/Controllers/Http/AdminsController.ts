import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import CreateAdminValidator from 'App/Validators/CreateAdminValidator'
import UpdateAdminValidator from 'App/Validators/UpdateAdminValidator'

export default class AdminsController {

  // admin
  public async index({ request, bouncer }: HttpContextContract) {
    await bouncer
      .with('AdminPolicy')
      .authorize('viewList')

    let page = request.input('page', 1)

    let admins = await User.query()
      .where('is_admin', 1)
      .orderBy('id', 'desc')
      .paginate(page, 10)

    return admins
  }

  // admin
  public async store ({ request, response, bouncer }: HttpContextContract) {
    await bouncer
      .with('AdminPolicy')
      .authorize('create')

    await request.validate(CreateAdminValidator)

    let admin = await User.create({
      name: request.input('name'),
      email: request.input('email'),
      password: request.input('password'),
      is_admin: true
    })

    return response.json({
      data: admin
    })
  }

  // admin
  public async show({ response, params, bouncer }: HttpContextContract) {
    await bouncer
      .with('AdminPolicy')
      .authorize('view')

    let admin = await User.query()
      .where('is_admin', 1)
      .where('id', params.id)
      .firstOrFail()

    return response.json({
      data: admin
    })
  }

  // admin
  public async update({ response, params, request, bouncer }: HttpContextContract) {
    await bouncer
      .with('AdminPolicy')
      .authorize('edit')

    await request.validate(UpdateAdminValidator)

    let admin = await User.query()
      .where('is_admin', 1)
      .where('id', params.id)
      .firstOrFail()

    admin.name = request.input('name')
    admin.email = request.input('email')
    if (request.input('password')) admin.password = request.input('password')
    await admin.save()

    return response.json({
      data: admin
    })
  }

  // admin
  public async destroy ({response, params, bouncer}: HttpContextContract) {
    await bouncer
      .with('AdminPolicy')
      .authorize('delete')

    let admin = await User.query()
      .where('is_admin', 1)
      .where('id', params.id)
      .firstOrFail()

    await admin.delete()

    return response.json({
      data: 'Admin deleted successfully'
    })

  }

}
