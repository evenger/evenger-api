import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Organizer from 'App/Models/Organizer'
import CreateOrganizerValidator from 'App/Validators/CreateOrganizerValidator'
import UpdateOrganizerValidator from 'App/Validators/UpdateOrganizerValidator'
import Drive from '@ioc:Adonis/Core/Drive'

export default class OrganizersController {

  /*
    - admin
    - operator hanya organizer di departmentnya saja
  */
  public async index({ request, bouncer, auth }: HttpContextContract) {
    await bouncer
      .with('OrganizerPolicy')
      .authorize('viewList')

    let page = request.input('page', 1)
    let name = request.input('name', '')

    let organizers = await Organizer.query()
      .withScopes((scope) => scope.visibleTo(auth.use('api').user))
      .preload('department')
      .if((name), (query) => {
        query.where('name', 'like', '%' + name + '%')
      })
      .orderBy('id', 'desc')
      .paginate(page, 10)

    return organizers
  }

  // operator
  public async store({ request, response, bouncer, auth }: HttpContextContract) {
    await bouncer
      .with('OrganizerPolicy')
      .authorize('create')

    await request.validate(CreateOrganizerValidator)

    let fileUpload = request.file('logo')
    await fileUpload.moveToDisk('local')

    let organizer = await Organizer.create({
      name: request.input('name'),
      desc: request.input('desc'),
      logo: fileUpload.filePath,
      departmentId: auth.use('api').user.departmentId
    })

    return response.json({
      data: organizer
    })
  }

  /*
    - admin
    - operator hanya organizer di departmentnya saja
    - user organizer hanya organizer yang terkait dengannya saja
  */
  public async show({ response, params, bouncer }: HttpContextContract) {
    let organizer = await Organizer.query()
      .preload('department')
      .where('id', params.id)
      .firstOrFail()

    await bouncer
      .with('OrganizerPolicy')
      .authorize('view', organizer)

    return response.json({
      data: organizer
    })
  }

  /*
  - operator
  - user organizer hanya organizer yang terkait dengannya saja
  */
  public async update({ response, params, request, bouncer }: HttpContextContract) {
    await request.validate(UpdateOrganizerValidator)

    let organizer = await Organizer.query()
      .where('id', params.id)
      .firstOrFail()

    await bouncer
      .with('OrganizerPolicy')
      .authorize('edit', organizer)

    if (await Drive.exists(organizer.logo)) await Drive.delete(organizer.logo)

    let fileUpload = request.file('logo')
    if (fileUpload){
      await fileUpload.moveToDisk('local')
      organizer.logo = fileUpload.filePath
    }

    organizer.name = request.input('name')
    organizer.desc = request.input('desc')
    await organizer.save()

    return response.json({
      data: organizer
    })
  }

  // operator
  public async destroy({ response, params, bouncer }: HttpContextContract) {
    await bouncer
      .with('OrganizerPolicy')
      .authorize('delete')

    let organizer = await Organizer.query()
      .where('id', params.id)
      .firstOrFail()

    if (await Drive.exists(organizer.logo)) await Drive.delete(organizer.logo)

    await organizer.delete()

    return response.json({
      data: 'Organizer deleted successfully'
    })

  }

}
