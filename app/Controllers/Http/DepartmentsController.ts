import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Department from 'App/Models/Department'
import DepartmentValidator from 'App/Validators/DepartmentValidator'

export default class DepartmentsController {

  // admin
  public async index({ request, bouncer }: HttpContextContract) {
    await bouncer
      .with('DepartmentPolicy')
      .authorize('viewList')

    let page = request.input('page', 1)
    let name = request.input('name', '')

    let departments = await Department.query()
      .orderBy('id', 'desc')
      .if((name), (query) => {
        query.where('name', 'like', '%' + name + '%')
      })
      .paginate(page, 10)

    return departments
  }

  // admin
  public async store({ request, response, bouncer }: HttpContextContract) {
    await bouncer
      .with('DepartmentPolicy')
      .authorize('create')

    await request.validate(DepartmentValidator)

    let department = await Department.create({
      name: request.input('name'),
      type: request.input('type'),
    })

    return response.json({
      data: department
    })
  }

  // admin
  public async show({ response, params, bouncer }: HttpContextContract) {
    await bouncer
      .with('DepartmentPolicy')
      .authorize('view')

    let department = await Department.findOrFail(params.id)

    return response.json({
      data: department
    })
  }

  // admin
  public async update({ response, params, request, bouncer }: HttpContextContract) {
    await bouncer
      .with('DepartmentPolicy')
      .authorize('edit')

    await request.validate(DepartmentValidator)

    let department = await Department.findOrFail(params.id)

    department.name = request.input('name')
    department.type = request.input('type')
    await department.save()

    return response.json({
      data: department
    })
  }

  // admin
  public async destroy({ response, params, bouncer }: HttpContextContract) {
    await bouncer
      .with('DepartmentPolicy')
      .authorize('delete')

    let department = await Department.findOrFail(params.id)

    await department.delete()

    return response.json({
      data: 'Department deleted successfully'
    })

  }
}
