import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Event from 'App/Models/Event'
import CreateEventValidator from 'App/Validators/CreateEventValidator'
import UpdateEventValidator from 'App/Validators/UpdateEventValidator'
import Drive from '@ioc:Adonis/Core/Drive'
import { schema } from '@ioc:Adonis/Core/Validator'
import { EventState } from 'Contracts/enums'

export default class EventsController {

  // all users & operator by department id
  public async index({ request, bouncer, auth }: HttpContextContract) {
    await bouncer
      .with('EventPolicy')
      .authorize('viewList')

    let page = request.input('page', 1)
    let name = request.input('name', '')

    let events = await Event.query()
      .withScopes((scope) => scope.visibleTo(auth.use('api').user))
      .preload('organizer', (qry) => {
        qry.preload('department')
      })
      .if((name), (query) => {
        query.where('name', 'like', '%' + name + '%')
      })
      .orderBy('id', 'desc')
      .paginate(page, 10)

    return events
  }

  // user organizer
  public async store({ request, response, bouncer, auth }: HttpContextContract) {
    await bouncer
      .with('EventPolicy')
      .authorize('create')

    await request.validate(CreateEventValidator)

    let fileUpload = request.file('poster')
    await fileUpload.moveToDisk('local')

    let event = await Event.create({
      name: request.input('name'),
      start: request.input('start'),
      end: request.input('end'),
      desc: request.input('desc'),
      poster: fileUpload.filePath,
      category: request.input('category'),
      organizerId: auth.use('api').user.organizerId,
    })

    return response.json({
      data: event
    })
  }

  // all users
  public async show({ response, params, bouncer }: HttpContextContract) {
    await bouncer
      .with('EventPolicy')
      .authorize('view')

    let event = await Event.query()
      .preload('organizer', (qry) => {
        qry.preload('department')
      })
      .preload('eventRegistrants')
      .where('id', params.id)
      .firstOrFail()

    return response.json({
      data: event
    })
  }

  // user organizer
  public async update({ response, params, request, bouncer }: HttpContextContract) {
    await request.validate(UpdateEventValidator)

    let event = await Event.query()
      .where('id', params.id)
      .firstOrFail()

    await bouncer
      .with('EventPolicy')
      .authorize('edit', event)

    if (await Drive.exists(event.poster)) await Drive.delete(event.poster)

    let fileUpload = request.file('poster')
    if (fileUpload) {
      await fileUpload.moveToDisk('local')
      event.poster = fileUpload.filePath
    }

    event.name = request.input('name')
    event.start = request.input('start')
    event.end = request.input('end')
    event.desc = request.input('desc')
    event.category = request.input('category')
    await event.save()

    return response.json({
      data: event
    })
  }

  // user organizer
  public async destroy({ response, params, bouncer }: HttpContextContract) {
    let event = await Event.query()
      .where('id', params.id)
      .firstOrFail()

    await bouncer
      .with('EventPolicy')
      .authorize('delete', event)

    if (await Drive.exists(event.poster)) await Drive.delete(event.poster)

    await event.delete()

    return response.json({
      data: 'Event deleted successfully'
    })
  }

  // operator
  public async validate({ response, request, bouncer }: HttpContextContract) {
    await bouncer
      .with('EventPolicy')
      .authorize('validate')

    let eventValidate = schema.create({
      state: schema.enum(Object.values(EventState)),
      stateDesc: schema.string.optional({ trim: true }),
    })

    await request.validate({ schema: eventValidate })

    let eventId = request.input('eventId')
    let state = request.input('state')
    let stateDesc = request.input('stateDesc')

    await Event.query()
      .whereIn('id', eventId)
      .update({
        state: state,
        state_desc: stateDesc,
      })

    return response.json({
      data: 'Event validated successfully'
    })
  }

}
