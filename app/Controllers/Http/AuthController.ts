import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class AuthController {

  public async login({ auth, request, response }: HttpContextContract) {
    const email = request.input('email')
    const password = request.input('password')

    try {
      const token = await auth.use('api').attempt(email, password, {
        expiresIn: '7days'
      })
      return response.json({
        data: token
      })
    } catch {
      return response.badRequest({
        error: 'Invalid credentials'
      })
    }
  }

  public async logout({ auth, response }: HttpContextContract){
    await auth.use('api').revoke()
    return response.json({
      revoked: true
    })
  }

}
