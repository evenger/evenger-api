import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import CreateUserOrganizerValidator from 'App/Validators/CreateUserOrganizerValidator'
import UpdateUserOrganizerValidator from 'App/Validators/UpdateUserOrganizerValidator'

export default class UserOrganizersController {
  public async index({ request, bouncer, auth }: HttpContextContract) {
    await bouncer
      .with('UserOrganizerPolicy')
      .authorize('viewList')

    let page = request.input('page', 1)
    let name = request.input('name', '')

    let userOrganizers = await User.query()
      .withScopes((scope) => scope.visibleTo(auth.use('api').user))
      .preload('organizer')
      .whereNotNull('organizer_id')
      .if((name), (query) => {
        query.where('name', 'like', '%' + name + '%')
      })
      .orderBy('id', 'desc')
      .paginate(page, 10)

    return userOrganizers
  }

  public async store({ request, response, bouncer }: HttpContextContract) {
    await bouncer
      .with('UserOrganizerPolicy')
      .authorize('create')

    await request.validate(CreateUserOrganizerValidator)

    let userOrganizer = await User.create({
      name: request.input('name'),
      email: request.input('email'),
      password: request.input('password'),
      organizerId: request.input('organizerId')
    })

    return response.json({
      data: userOrganizer
    })
  }

  public async show({ response, params, bouncer, auth }: HttpContextContract) {
    let userOrganizer = await User.query()
      .withScopes((scope) => scope.visibleTo(auth.use('api').user))
      .preload('organizer')
      .whereNotNull('organizer_id')
      .where('id', params.id)
      .firstOrFail()

    await bouncer
      .with('UserOrganizerPolicy')
      .authorize('view', userOrganizer)

    return response.json({
      data: userOrganizer
    })
  }

  public async update({ response, params, request, bouncer }: HttpContextContract) {
    await request.validate(UpdateUserOrganizerValidator)

    let userOrganizer = await User.query()
      .whereNotNull('organizer_id')
      .where('id', params.id)
      .firstOrFail()

    await bouncer
      .with('UserOrganizerPolicy')
      .authorize('edit', userOrganizer)

    userOrganizer.name = request.input('name')
    userOrganizer.email = request.input('email')
    userOrganizer.organizerId = request.input('organizerId')
    if (request.input('password')) userOrganizer.password = request.input('password')
    await userOrganizer.save()

    return response.json({
      data: userOrganizer
    })
  }

  public async destroy({ response, params, bouncer }: HttpContextContract) {
    await bouncer
      .with('UserOrganizerPolicy')
      .authorize('delete')

    let userOrganizer = await User.query()
      .whereNotNull('organizer_id')
      .where('id', params.id)
      .firstOrFail()

    await userOrganizer.delete()

    return response.json({
      data: 'User Organizer deleted successfully'
    })

  }

}
