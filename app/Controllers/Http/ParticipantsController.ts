import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import CreateParticipantValidator from 'App/Validators/CreateParticipantValidator'
import UpdateParticipantValidator from 'App/Validators/UpdateParticipantValidator'

export default class ParticipantsController {

  // admin
  public async index({ request, bouncer }: HttpContextContract) {
    await bouncer
      .with('ParticipantPolicy')
      .authorize('viewList')

    let page = request.input('page', 1)
    let name = request.input('name', '')

    let participants = await User.query()
      .where('is_admin', 0)
      .whereNull('organizer_id')
      .whereNull('department_id')
      .if((name), (query) => {
        query.where('name', 'like', '%' + name + '%')
      })
      .orderBy('id', 'desc')
      .paginate(page, 10)

    return participants
  }

  // all users
  public async show({ response, params, bouncer }: HttpContextContract) {
    await bouncer
      .with('ParticipantPolicy')
      .authorize('view')

    let participant = await User.query()
      .preload('eventRegistrants')
      .where('is_admin', 0)
      .whereNull('organizer_id')
      .whereNull('department_id')
      .where('id', params.id)
      .firstOrFail()

    return response.json({
      data: participant
    })
  }

  // only guest (no login)
  public async store({ request, response, auth }: HttpContextContract) {
    await request.validate(CreateParticipantValidator)

    await User.create({
      name: request.input('name'),
      email: request.input('email'),
      password: request.input('password'),
    })

    const token = await auth.use('api').attempt(
      request.input('email'),
      request.input('password'),{
      expiresIn: '7days'
    })

    return response.json({
      data: token
    })

  }

  // participant
  public async update({ response, params, request, bouncer }: HttpContextContract) {
    await request.validate(UpdateParticipantValidator)

    let participant = await User.query()
      .where('is_admin', 0)
      .whereNull('organizer_id')
      .whereNull('department_id')
      .where('id', params.id)
      .firstOrFail()

    await bouncer
      .with('ParticipantPolicy')
      .authorize('edit', participant)

    participant.name = request.input('name')
    participant.email = request.input('email')
    if (request.input('password')) participant.password = request.input('password')
    await participant.save()

    return response.json({
      data: participant
    })
  }

  // admin
  public async destroy({ response, params, bouncer }: HttpContextContract) {
    await bouncer
      .with('ParticipantPolicy')
      .authorize('delete')

    let participant = await User.query()
      .where('is_admin', 0)
      .whereNull('organizer_id')
      .whereNull('department_id')
      .where('id', params.id)
      .firstOrFail()

    await participant.delete()

    return response.json({
      data: 'Participant deleted successfully'
    })

  }
}
