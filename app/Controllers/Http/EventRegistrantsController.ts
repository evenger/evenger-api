import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import EventRegistrant from 'App/Models/EventRegistrant'
import Drive from '@ioc:Adonis/Core/Drive'
import EventRegistrantValidator from 'App/Validators/EventRegistrantValidator'
import { schema } from '@ioc:Adonis/Core/Validator'
import { EventRegistrantState } from 'Contracts/enums'

export default class EventRegistrantsController {

  // all user
  public async index({ request, bouncer }: HttpContextContract) {
    await bouncer
      .with('RegistrantPolicy')
      .authorize('viewList')

    let page = request.input('page', 1)
    let eventId = request.input('event_id')

    let registrants = await EventRegistrant.query()
      .preload('event')
      .preload('user')
      .where('event_id', eventId)
      .orderBy('id', 'desc')
      .paginate(page, 10)

    return registrants
  }

  // participant
  public async store({ request, response, bouncer, auth }: HttpContextContract) {
    await bouncer
      .with('RegistrantPolicy')
      .authorize('create')

    await request.validate(EventRegistrantValidator)

    let registrant = await EventRegistrant.create({
      eventId: request.input('eventId'),
      userId: auth.use('api').user.id,
    })

    return response.json({
      data: registrant
    })
  }

  // participant
  public async destroy({ response, params, bouncer }: HttpContextContract) {
    let registrant = await EventRegistrant.query()
      .where('id', params.id)
      .firstOrFail()

    await bouncer
      .with('RegistrantPolicy')
      .authorize('delete', registrant)

    await registrant.delete()

    return response.json({
      data: 'Event Registrant deleted successfully'
    })
  }

  // user organizer
  public async validate({ response, request, bouncer }: HttpContextContract) {
    await bouncer
      .with('RegistrantPolicy')
      .authorize('validate')

    let registrantValidate = schema.create({
      state: schema.enum(Object.values(EventRegistrantState)),
      stateDesc: schema.string.optional({ trim: true }),
    })

    await request.validate({ schema: registrantValidate })

    let registrantId = request.input('registrantId')
    let state = request.input('state')
    let stateDesc = request.input('stateDesc')

    await EventRegistrant.query()
      .whereIn('id', registrantId)
      .update({
        state: state,
        state_desc: stateDesc,
      })

    return response.json({
      data: 'Registrant validated successfully'
    })
  }

  // user organizer
  public async submitResult({ response, request, bouncer }: HttpContextContract) {
    await bouncer
      .with('RegistrantPolicy')
      .authorize('submitResult')

    let resultValidate = schema.create({
      resultFile: schema.file.optional({
        size: '5mb',
        extnames: ['jpg', 'gif', 'png'],
      }),
      resultDesc: schema.string.optional({ trim: true }),
    })

    await request.validate({ schema: resultValidate })

    let fileUpload = request.file('resultFile')
    await fileUpload.moveToDisk('local')

    let registrantId = request.input('registrantId')
    let resultDesc = request.input('resultDesc')


    let registrants = await EventRegistrant.query()
      .whereIn('id', registrantId)

    registrants.forEach(async (registrant) => {
      if (await Drive.exists(registrant.resultFile)) await Drive.delete(registrant.resultFile)
    });

    await EventRegistrant.query()
      .whereIn('id', registrantId)
      .update({
        result_desc: resultDesc,
        result_file: fileUpload.filePath
      })

    return response.json({
      data: 'Result event submitted successfully'
    })
  }

}
