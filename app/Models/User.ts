import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column, hasMany, HasMany, beforeSave, scope } from '@ioc:Adonis/Lucid/Orm'
import Organizer from './Organizer'
import Department from './Department'
import EventRegistrant from './EventRegistrant'
import Hash from '@ioc:Adonis/Core/Hash'

export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column()
  public is_admin: boolean

  @column()
  public rememberMeToken?: string

  @column()
  public organizerId: number

  @column()
  public departmentId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Organizer)
  public organizer: BelongsTo<typeof Organizer>

  @belongsTo(() => Department)
  public department: BelongsTo<typeof Department>

  @hasMany(() => EventRegistrant)
  public eventRegistrants: HasMany<typeof EventRegistrant>

  @beforeSave()
  public static async hashPassword(user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }

  public static visibleTo = scope((query, user: User) => {
    if (user.departmentId) {
      return query.whereHas('organizer', (qry) => {
        qry.where('department_id', user.departmentId)
      })
    } else if (user.is_admin) {
      return
    }
  })

}
