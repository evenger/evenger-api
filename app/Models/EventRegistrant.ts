import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import { EventRegistrantState } from 'Contracts/enums'
import Event from './Event'
import User from './User'

export default class EventRegistrant extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public state: EventRegistrantState

  @column()
  public stateDesc: string

  @column()
  public resultDesc: string

  @column()
  public resultFile: string

  @column()
  public eventId: number

  @column()
  public userId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Event)
  public event: BelongsTo<typeof Event>

  @belongsTo(() => User)
  public user: BelongsTo<typeof User>

}
