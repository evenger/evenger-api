import { DateTime } from 'luxon'
import { BaseModel, column, HasOne, hasOne, belongsTo, BelongsTo, hasMany, HasMany, scope } from '@ioc:Adonis/Lucid/Orm'
import User from './User'
import Department from './Department'
import Event from './Event'

export default class Organizer extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public desc: string

  @column()
  public logo: string

  @column()
  public departmentId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasOne(() => User)
  public user: HasOne<typeof User>

  @belongsTo(() => Department)
  public department: BelongsTo<typeof Department>

  @hasMany(() => Event)
  public events: HasMany<typeof Event>

  public static visibleTo = scope((query, user: User) => {
    if (user.departmentId) {
      return query.where('department_id', user.departmentId)
    } else if(user.is_admin){
      return
    }
  })

}
