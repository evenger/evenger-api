import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo, HasMany, hasMany, scope } from '@ioc:Adonis/Lucid/Orm'
import Organizer from './Organizer'
import EventRegistrant from './EventRegistrant'
import { EventState } from 'Contracts/enums'
import User from './User'

export default class Event extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public start: DateTime

  @column()
  public end: DateTime

  @column()
  public desc: string | null

  @column()
  public poster: string

  @column()
  public category: string

  @column()
  public state: EventState

  @column()
  public stateDesc: string

  @column()
  public organizerId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Organizer)
  public organizer: BelongsTo<typeof Organizer>

  @hasMany(() => EventRegistrant)
  public eventRegistrants: HasMany<typeof EventRegistrant>

  public static visibleTo = scope((query, user: User) => {
    if (user.departmentId) {
      return query.whereHas('organizer', (qry) => {
        qry.where('department_id', user.departmentId)
      })
    } else {
      return
    }
  })

}
