import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'

export default class OperatorPolicy extends BasePolicy {
	public async viewList(user: User) {
    return user.is_admin === 1
  }

  public async create(user: User) {
    return user.is_admin === 1
  }

  // admin & operator itu sendiri
  public async view(user: User, operator: User){
    return user.is_admin === 1 || user.id === operator.id
  }

  // admin & operator itu sendiri
  public async edit(user: User, operator: User) {
    return user.is_admin === 1 || user.id === operator.id
  }

  // admin
  public async delete(user: User) {
    return user.is_admin === 1
  }

}
