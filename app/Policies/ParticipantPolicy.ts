import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'

export default class ParticipantPolicy extends BasePolicy {
  // admin
  public async viewList(user: User) {
    return user.is_admin === 1
  }

  // all users
  public async view(user: User) {
    return user.id !== null
  }

  // participant
  public async edit(user: User, participant: User) {
    return user.id === participant.id
  }

  // admin
  public async delete(user: User) {
    return user.is_admin === 1
  }

}
