import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'
import Organizer from 'App/Models/Organizer'

export default class OrganizerPolicy extends BasePolicy {
  /*
    - admin
    - operator hanya organizer di departmentnya saja
  */
  public async viewList(user: User) {
    return user.is_admin === 1 || user.departmentId !== null
  }

  /*
    - admin
    - operator hanya organizer di departmentnya saja
    - user organizer hanya organizer yang terkait dengannya saja
  */
  public async view(user: User, organizer: Organizer) {
    return user.is_admin === 1 || user.departmentId === organizer.departmentId || user.organizerId === organizer.id
  }

  // operator
  public async create(user: User) {
    return user.departmentId !== null
  }

  /*
  - operator
  - user organizer hanya organizer yang terkait dengannya saja
  */
  public async edit(user: User, organizer: Organizer) {
    return user.departmentId !== null || user.organizerId === organizer.id
  }

  // operator
  public async delete(user: User) {
    return user.departmentId !== null
  }

}
