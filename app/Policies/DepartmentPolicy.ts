import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'

export default class DepartmentPolicy extends BasePolicy {
  public async viewList(admin: User) {
    return admin.is_admin === 1
  }

  public async view(admin: User) {
    return admin.is_admin === 1
  }

  public async create(admin: User) {
    return admin.is_admin === 1
  }

  public async edit(admin: User) {
    return admin.is_admin === 1
  }

  public async delete(admin: User) {
    return admin.is_admin === 1
  }

}
