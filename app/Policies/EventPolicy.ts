import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'
import Event from 'App/Models/Event'

export default class EventPolicy extends BasePolicy {

  /*
    all users &
    operator by department id
   */
  public async viewList(user: User) {
    return user.id !== null
  }

  // all users
  public async view(user: User) {
    return user.id !== null
  }

  // user organizer
  public async create(user: User) {
    return user.organizerId !== null
  }

  // user organizer
  public async edit(user: User, event: Event) {
    return user.organizerId === event.organizerId
  }

  // user organizer
  public async delete(user: User, event: Event) {
    return user.organizerId === event.organizerId
  }

  // operator
  public async validate(user: User) {
    return user.departmentId !== null
  }

}
