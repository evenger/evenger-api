import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'

export default class UserOrganizerPolicy extends BasePolicy {
  /*
    - admin
    - operator
  */
  public async viewList(user: User) {
    return user.is_admin === 1 || user.departmentId !== null
  }

  /*
    - admin
    - operator
    - user organizer itu sendiri
  */
  public async view(user: User, userOrganizer: User) {
    return user.is_admin === 1 || user.departmentId !== null || user.organizerId === userOrganizer.organizerId
  }

  // operator
  public async create(user: User) {
    return user.departmentId !== null
  }

  /*
    - operator
    - user organizer itu sendiri
  */
  public async edit(user: User, userOrganizer: User) {
    return user.departmentId !== null || user.organizerId === userOrganizer.organizerId
  }

  // operator
  public async delete(user: User) {
    return user.departmentId !== null
  }

}
