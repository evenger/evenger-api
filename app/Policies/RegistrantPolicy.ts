import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'
import EventRegistrant from 'App/Models/EventRegistrant'

export default class RegistrantPolicy extends BasePolicy {
  /*
    all users
    operator by department id
   */
  public async viewList(user: User) {
    return user.id !== null
  }

  // user organizer
  public async create(user: User) {
    return user.organizerId === null && user.is_admin === 0 && user.departmentId === null
  }

  // participant
  public async delete(user: User, registrant: EventRegistrant) {
    return user.id === registrant.userId
  }

  // user organizer
  public async validate(user: User) {
    return user.organizerId !== null
  }

  // user organizer
  public async submitResult(user: User) {
    return user.organizerId !== null
  }

}
