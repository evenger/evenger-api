export enum EventState {
  APPROVED = 'approved',
  REJECTED = 'rejected',
  PROCESSED = 'processed'
}

export enum EventRegistrantState {
  APPROVED = 'approved',
  ACCEPTED = 'accepted',
  REJECTED = 'rejected',
  PROCESSED = 'processed',
  CANCELED = 'canceled'
}

export enum DepartmentType {
  UNIVERSITY = 'universitas',
  FACULTY = 'fakultas',
  DEPARTMENT = 'jurusan',
  STUDY_PROGRAM = 'program studi',
  EXTERNAL = 'external',
}
